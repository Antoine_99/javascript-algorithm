function analyser_Tableau(tableau = new Array(9)){
    for(i=0; i<tableau.length; i++)
    {
        /*On vérifie que le tableau contient uniquement des chiffres différents*/
        if(!tableau[i].includes('1'||'2'||'3'||'4'||'5'||'6'||'7'||'8'||'9') && tableau[i].charAt(i) === tableau[i].charAt(i+1))
            {
                return false;
            }
        else
            {
                return true;
            }
    }
    
}